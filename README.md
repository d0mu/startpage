# startpage

Minimalistic "Startpage" for my personal use. With multiple themes.

- Font: SpaceMono Nerd Font
- Colour Scheme(s): Tokyo Night Colours

URL: https://d0mu.gitlab.io/startpage/domu/

----

Credits:
- https://gitlab.com/fazzi/startpage for the template
- https://reddit.com/r/startpages


*Images are NOT my own. Full rights to original image owners*

